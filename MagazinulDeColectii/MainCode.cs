﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagazinulDeColectii
{
    public class MainCode
    {
        
        IWebDriver driver;
    }

    //Declaring login Elements
    public class LoginPage

    {
        [FindsBy(How = How.ClassName, Using = "first last")]
        IWebElement authButton;

        [FindsBy(How = How.Id, Using = "email")]
        IWebElement emailUser;

        [FindsBy(How = How.Id, Using = "pass")]
        IWebElement passUser;

        [FindsBy(How = How.ClassName, Using = "actions")]
        IWebElement forgotPass;

        [FindsBy(How = How.XPath, Using = "send2")]
        IWebElement loginButton;

        [FindsBy(How = How.XPath, Using = "login-form")]
        IWebElement regButton;
    }

    public class RegistrationElements
    {

    }
    
}
